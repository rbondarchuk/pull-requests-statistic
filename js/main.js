$(document).ready(function () {

    var values;
    var datas = [];
    var labels = [];

    $.ajax({
        url: 'https://bitbucket.org/api/2.0/repositories/connecture/client-uhg/pullrequests/',
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (data) {
            values = data.values || [];
        },
        beforeSend: setBasicAuth
    });

    _.forEach(values, function (value) {

        $.ajax({
            url: value.links.comments.href,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function (data) {
                datas.push(data.size || 0);
            },
            beforeSend: setBasicAuth
        });

        labels.push(value.author.username);
    });
    var data = {
        labels: labels,
        datasets: [{
            data: datas
        }]
    };

    var ctx = $("#activityChart").get(0).getContext("2d");
    var myBarChart = new Chart(ctx).Bar(data, {});
    myBarChart.draw();

});

function setBasicAuth(xhr) {
    xhr.setRequestHeader("Authorization", "Basic " + btoa("login:password"));
}
